# INDEXER UN FICHIER
git add [nomficher]

# INDEXER TOUS LES FICHIERs DU DOSSIER COURRANT ET SES ENFANTS
git add .

# COMMITER SUR LA BRANCHE EN LOCAL
git commit -m "[messageobligatoire]"

# TIRER 
git pull

# POUR POUSSER SUR BITBUCKET
git push 

# RESTAUTATION A LA Tête de version
git reset --hard HEAD

# CREER UNE BRANCHE RAPIDEMENT
1. Se mettre sur la branche source
1. `git checkout -b nom-branche`
1. `git push --set-upstream origin nom-branche`

# CREER BRANCHE 
1. Se mettre sur la branche source
1. `git branch feat-exemple`
1. `git push --set-upstream origin feat-exemple`
1. changer de branche

# SWITCH BRANCH
git checkout master/feat-exemple
ou
git checkout feat-exemple

# VOIR L'HISTORIQUE
GIT LOG

# voir les modifications courantes (indéxé en vert et non indéxé en rouge)
git status

# Pousser dans le Stash
git stash push -m "message"

# Récupérer du stash
git stash pop --index index

# List de ce qu'il y a en Stash
git stash list

# Déplacer des commits non-pushé, d'une branche à une autre
Dans notre exemple, on veut déplacer des commit sur master vers mybranch (qui n'existe pas)
1. Create new branch with your changes.`git checkout -b mybranch`
1. (Optional) Push new branch code on remote server. `git push origin mybranch`
1. Checkout back to master branch. `git checkout master`
1. Reset master branch code with remote server and remove local commit. `git reset --hard origin/master`

# Fusionner d'une branche parent à une branche enfant sans faire de remoue
1. Sur la branche source `git pull -r`
1. Sur la branche cible `git rebase branche-source`
1. Il devrait y avoir des choses à tirer et à pousser. Donc `git push --force`

Normalement cette méthode met les commits de la branche source avant les nôtres. Et ne crée pas de commit de merge.

# Récupérer juste quelques commits d'une autre branche
https://www.youtube.com/watch?v=_7Q0D_Y50LM 